let math = {
  "toDegrees": function (angle) {
    return angle * (Math.PI / 180);
  },
  "toRadians": function (angle) {
    return angle * (180 / Math.PI);
  },
}

let physics = {
  "force": function (mass, acceleration) {
    return mass * acceleration;
  },
  "work": function (force, distance, angle=0) {
    return force * distance * Math.cos(math.toDegrees(angle));
  },
  "power": function (work, time) {
    return work / time;
  },
  "density": function (mass, velocity) {
    return mass / velocity;
  },
  "volume": function (mass, density) {
    return mass / density;
  },
  "mass": function (density, volume) {
    return density * volume;
  }
}